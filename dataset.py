import glob
from pathlib import Path
import os
import math
import numpy as np
import json
import pickle
from bbox import BBox
from PIL import Image
import model
import json
import warnings

# Raw dataset samples directory
DATASET_DIR = "/mnt/storage_hdd/deep learning/datasets/basic_platform_50k_batch/"

# Where to cache parsed dataset
DETECTION_DATASET_CACHE = "training/detection_25k.cache.pkl"
POSENET_DATASET_CACHE = "training/posenet_25k.cache.pkl"

# -1 to remove limit
DATASET_LIMIT = 24960

DEPTH_ORIGINAL_SIZE = 512

# Set boundaries (in %) for bbox position, size, etc inside the image
MIN_BBOX_COORD = 0
MAX_BBOX_COORD = 1
MIN_BBOX_SIZE = 0.02
MAX_BBOX_SIZE = 0.8
MIN_BBOX_CENTER = 0.05
MAX_BBOX_CENTER = 0.95

# Intrinsic camera principal point offset
CX = 256
CY = 256


def remove_outliers():
    samples = get_samples(DATASET_DIR)

    for sample in samples:
        # Process BBOX
        bbox = BBox(
            sample['objects'][0]['bbox'],
            import_format="classic",
            img_width=DEPTH_ORIGINAL_SIZE
        )

        if not _check_if_outlier(bbox):
            continue

        # BBox is outlier
        print("Sample {} marked as outlier {}".format(sample["file_id"], bbox))
        _remove_outlier(sample)


def get_samples(ds_dir):
    """
    Get paths for all NDDS samples in the dataset directory
    """
    samples = []
    for curr_file_id in _get_file_ids(ds_dir):
        filepath_no_ext = os.path.join(ds_dir, curr_file_id)
        json_path = filepath_no_ext + ".json"
        objects = _parse_annotation_file(json_path)

        if len(objects) > 0:
            samples.append({
                'file_id': curr_file_id,
                'rgb': filepath_no_ext + ".png",
                'depth_mm_16': filepath_no_ext + ".depth.mm.16.png",
                'pred_tf_bbox': _parse_predicted_bbox(filepath_no_ext + ".pred.json"),
                'objects': objects,
            })
    return samples


def prepare_as_detection_io(samples):
    # Inputs
    depth_maps = []
    bboxes = []

    sample_count = len(samples)

    for i, sample in enumerate(samples):
        # Log progress
        if i % 100 == 0:
            print("Importing sample {} [{}%]".format(
                sample["file_id"], int(i/sample_count*100)))

        # Process BBOX
        bbox = BBox(
            sample['objects'][0]['bbox'],
            import_format="classic",
            img_width=DEPTH_ORIGINAL_SIZE
        )
        # bbox.clip_to_image_bounds()

        # Process DEPTH
        img = Image.open(sample['depth_mm_16'])
        img = img.resize(
            (model.RGB_IMG_SIZE, model.RGB_IMG_SIZE), Image.NEAREST)
        raw_depth = np.array(img, dtype=np.uint16)
        raw_depth, _, _ = _normalize_depth_map(raw_depth)
        img.close()

        # Append sample
        depth_maps.append(raw_depth)
        bboxes.append(bbox.to_tf())

    return {
        'depth': np.asarray(depth_maps).reshape((-1, model.RGB_IMG_SIZE, model.RGB_IMG_SIZE, 1))
    }, {
        'bbox': np.asarray(bboxes)
    }


def prepare_as_posenet_io(samples):
    """
    Process and convert dataset in a format that can be feeded to the model.
    Cuts depth maps using predicted bboxes instead of ground truth.
    """
    cropped_depth_maps = []
    crop_metadata = []
    centroids = []
    quaternions = []

    sample_count = len(samples)

    for i, sample in enumerate(samples):
        # Log progress
        if i % 100 == 0:
            print("Importing sample {} [{}%]".format(
                sample["file_id"], int(i/sample_count*100)))

        # Process predicted BBOX
        bbox = BBox(sample['pred_tf_bbox'], import_format="tf")
        bbox.set_img_size(DEPTH_ORIGINAL_SIZE)
        bbox.resize_as_square()

        # Process DEPTH
        img = Image.open(sample['depth_mm_16'])
        img = img.crop(bbox.to_classic())
        img = img.resize((model.CROPPED_DEPTH_IMG_SIZE,
                          model.CROPPED_DEPTH_IMG_SIZE), Image.NEAREST)
        raw = np.array(img, dtype=np.uint16)
        img.close()
        raw, mean, std = _normalize_depth_map(raw)
        delta_cx, delta_cy, scaling = _calc_slice_intrinsic(bbox)
        crop_meta = np.array([delta_cx, delta_cy, scaling, mean, std])

        # Append sample
        cropped_depth_maps.append(raw)
        crop_metadata.append(crop_meta)
        centroids.append(np.asarray(sample['objects'][0]['centroid']))
        quaternions.append(np.asarray(sample['objects'][0]['quaternion_xyzw']))

    return {
        'depth_cropped': np.asarray(cropped_depth_maps).reshape((-1, model.CROPPED_DEPTH_IMG_SIZE, model.CROPPED_DEPTH_IMG_SIZE, 1)),
        'crop_metadata': np.asarray(crop_metadata)
    }, {
        'centroid': np.asarray(centroids),
        'quaternion_xyzw': np.asarray(quaternions),
    }


def read_cached_detection_io():
    """
    Read cached dataset. If not present, cache it.
    """
    while True:
        try:
            with open(DETECTION_DATASET_CACHE, 'rb') as f:
                print("Reading cached dataset")
                return pickle.load(f)
        except FileNotFoundError:
            print("No cached dataset found. Creating it now...")
            _write_detection_io_cache()


def read_cached_posenet_io():
    """
    Read cached dataset. If not present, cache it.
    """
    while True:
        try:
            with open(POSENET_DATASET_CACHE, 'rb') as f:
                print("Reading cached dataset")
                return pickle.load(f)
        except FileNotFoundError:
            print("No cached dataset found. Creating it now...")
            _write_posenet_io_cache()


def write_predicted_bboxes(samples, yp, dataset_dir):
    for i, sample in enumerate(samples):
        file_id = sample["file_id"]
        tf_bbox = yp[i].tolist()

        # Write pred to file
        filename = os.path.join(dataset_dir, file_id + ".pred.json")
        with open(filename, 'w') as f:
            json.dump(tf_bbox, f)


def _remove_outlier(sample):
    # Find all sample files
    files = glob.glob1(DATASET_DIR, "{}.*".format(sample["file_id"]))

    # Create _outliers dir
    dest_dir = DATASET_DIR + "_outliers/"
    Path(dest_dir).mkdir(parents=False, exist_ok=True)

    # Move files into dir
    for file_name in files:
        os.rename(DATASET_DIR + file_name, dest_dir + file_name)


def _check_if_outlier(bbox: BBox):
    """
    Check if bbox is too small, too big or out of image bounds by a certain amount
    """

    # Check if too small or too big
    w, h = bbox.get_width(), bbox.get_height()
    is_valid = MIN_BBOX_SIZE <= min(w, h) and max(w, h) <= MAX_BBOX_SIZE

    # Check center is inside the image
    cx, cy = bbox.get_center()
    is_valid = is_valid and MIN_BBOX_CENTER < cx < MAX_BBOX_CENTER
    is_valid = is_valid and MIN_BBOX_CENTER < cy < MAX_BBOX_CENTER
    return not is_valid


def _write_detection_io_cache():
    """
    Update cached dataset file
    """
    print("Writing dataset to cache...")
    samples = get_samples(DATASET_DIR)
    input_data, output_data = prepare_as_detection_io(
        samples)
    with open(DETECTION_DATASET_CACHE, 'wb') as f:
        pickle.dump([input_data, output_data], f)
    print("Done.")


def _write_posenet_io_cache():
    """
    Update cached dataset file
    """
    print("Writing dataset to cache...")
    samples = get_samples(DATASET_DIR)
    input_data, output_data = prepare_as_posenet_io(samples)
    with open(POSENET_DATASET_CACHE, 'wb') as f:
        pickle.dump([input_data, output_data], f)
    print("Done.")


def _parse_annotation_file(json_file_path):
    """
    Parse ground truth from JSON file and return it as array
    Returns: array of as many elements as actors in the scene. [] if there are no actors.
    """
    with open(json_file_path) as json_data:
        objects = []
        for obj in json.load(json_data)["objects"]:
            [y0, x0] = obj["bounding_box"]["top_left"]  # x e y invertiti
            [y1, x1] = obj["bounding_box"]["bottom_right"]
            obj_index = 0  # in the future we may recognize more object categories
            quaternion_xyzw = obj["quaternion_xyzw"]
            centroid = obj["cuboid_centroid"]

            objects.append({
                'obj_index': obj_index,
                'bbox': [x0, y0, x1, y1],
                'centroid': centroid,
                'quaternion_xyzw': quaternion_xyzw
            })
        return objects


def _parse_predicted_bbox(json_file_path):
    """
    Return array tf format representing the predicted bounding box
    """
    if not os.path.isfile(json_file_path):
        return

    with open(json_file_path) as json_data:
        tf_bbox = json.load(json_data)
        return tf_bbox


def _get_file_ids(ds_dir):
    """
    Return list of all samples ids in the dataset directory
    e.g. ["000123", "000124"] for samples 000123.png, 000124.png
    """
    imgs = glob.glob1(ds_dir, "??????.png")
    ids = list(set([filename[0:6] for filename in imgs]))
    ids.sort()
    return ids[0:DATASET_LIMIT]


def _normalize_depth_map(raw):
    """
    Normalize image to have mean 0 and std 1
    """
    raw = raw.astype(np.float32)
    mean = np.mean(raw)
    std = np.std(raw)
    raw = (raw - mean)
    if std != 0:
        raw = raw / std
    else:
        warnings.warn("variance=0 in sample")
    return raw, mean, std


def _calc_slice_intrinsic(bbox: BBox):
    [x0, y0, _, _] = bbox.to_classic()
    l = max(bbox.get_width_px(), bbox.get_height_px())
    delta_cx = CX - x0
    delta_cy = CY - y0
    scaling = l / model.CROPPED_DEPTH_IMG_SIZE
    return delta_cx, delta_cy, scaling

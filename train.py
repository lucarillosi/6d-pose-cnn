import dataset
import keras
import model as mdl
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau, TensorBoard
from keras import layers, utils, optimizers, metrics
import numpy as np
from progress_tracker import ProgressTracker
import keras.backend as K
import tensorflow as tf
from datetime import datetime
from pathlib import Path
import os
import sys

BATCH_SIZE = 128
MAX_EPOCHS = 400
EARLY_STOP = 50
LOG_DIR_BASE = "training/logs"


def quaternion_loss(yt, yp):
    yt = K.l2_normalize(yt, axis=-1)
    yp = K.l2_normalize(yp, axis=-1)
    dot_prod = K.sum(yt * yp, axis=-1, keepdims=True)
    p = K.square(dot_prod) * 2 - 1
    p = p * 0.9999999
    q_angle_diff = tf.math.acos(p)
    return K.square(q_angle_diff)


def train_position(log_dir, plateau_patience, plateau_factor):
    # 0.001 is a good initial LR for the initial untrained model
    # 0.0007 is a good initial LR to fine tune
    lr = 0.0007

    model = mdl.position_model()
    model.load_weights(
        "training/logs/20200708_201512_position/weights.hdf5")
    model.compile(
        loss="mean_squared_error",
        optimizer=optimizers.Adam(learning_rate=lr)
    )
    utils.plot_model(model, to_file=log_dir + "model.png",
                     show_shapes=True, show_layer_names=True)

    # Init callbacks
    pt = ProgressTracker()
    tensor_board = TensorBoard(log_dir=log_dir, histogram_freq=1, write_graph=False,
                               write_images=True, write_grads=True, update_freq="epoch", embeddings_freq=1)
    checkpoint = ModelCheckpoint(log_dir + "weights.hdf5", monitor="val_loss",
                                 verbose=1, save_best_only=True, save_weights_only=True, mode="min")
    stop = EarlyStopping(monitor="val_loss", patience=EARLY_STOP, mode="min")
    reduce_lr = ReduceLROnPlateau(monitor="val_loss", factor=plateau_factor,
                                  patience=plateau_patience, min_lr=1e-7, verbose=1, mode="min")

    input_data, output_data = dataset.read_cached_posenet_io()
    history = model.fit(
        x=input_data,
        y=output_data['centroid'],
        batch_size=BATCH_SIZE,
        epochs=MAX_EPOCHS,
        verbose=1,
        callbacks=[checkpoint, reduce_lr, stop, tensor_board, pt],
        validation_split=0.2,
        shuffle=True
    )
    return history


def train_rotation(log_dir, plateau_patience, plateau_factor):
    # 0.001 is a good LR for the untrained model
    lr = 0.0007

    model = mdl.rotation_model()
    model.load_weights("training/logs/20200707_194449_rotation/weights.hdf5")
    model.compile(
        loss=quaternion_loss,
        optimizer=optimizers.Adam(learning_rate=lr)
    )
    utils.plot_model(model, to_file=log_dir + "model.png",
                     show_shapes=True, show_layer_names=True)

    # Init callbacks
    pt = ProgressTracker()
    tensor_board = TensorBoard(log_dir=log_dir, histogram_freq=1, write_graph=False,
                               write_images=True, write_grads=True, update_freq="epoch", embeddings_freq=1)
    checkpoint = ModelCheckpoint(log_dir + "weights.hdf5", monitor="val_loss",
                                 verbose=1, save_best_only=True, save_weights_only=True, mode="min")
    stop = EarlyStopping(monitor="val_loss", patience=EARLY_STOP, mode="min")
    reduce_lr = ReduceLROnPlateau(monitor="val_loss", factor=plateau_factor,
                                  patience=plateau_patience, min_lr=1e-7, verbose=1, mode="min")

    input_data, output_data = dataset.read_cached_posenet_io()
    history = model.fit(
        x=input_data,
        y=output_data['quaternion_xyzw'],
        batch_size=BATCH_SIZE,
        epochs=MAX_EPOCHS,
        verbose=1,
        callbacks=[checkpoint, reduce_lr, stop, tensor_board, pt],
        validation_split=0.2,
        shuffle=True
    )
    return history


def iou(yt, yp):
    """
    Compute intersection over union metric
    Accepts tensors of bounding boxes expressed in the format [y1, x1, y2, x2]
    """

    # determine the (x, y)-coordinates of the intersection rectangle
    yA = K.maximum(yt[:, 0], yp[:, 0])
    xA = K.maximum(yt[:, 1], yp[:, 1])
    yB = K.minimum(yt[:, 2], yp[:, 2])
    xB = K.minimum(yt[:, 3], yp[:, 3])

    # compute the area of the intersection
    intersect_area = K.maximum(0.0, xB - xA) * K.maximum(0.0, yB - yA)

    # compute the area of both the prediction and ground-truth rectangles
    yt_area = (yt[:, 3] - yt[:, 1]) * (yt[:, 2] - yt[:, 0])
    yp_area = (yp[:, 3] - yp[:, 1]) * (yp[:, 2] - yp[:, 0])

    # compute ratio adding a small value to avoid division by 0
    return intersect_area / (yt_area + yp_area - intersect_area + 1e-07)


def train_detection(log_dir, lr, plateau_patience, plateau_factor):
    lr = 0.00002
    model = mdl.detection_module()
    model.load_weights("training/logs/20200727_133134_detection/weights.hdf5")
    model.compile(
        loss="mean_squared_error",
        optimizer=optimizers.Adam(learning_rate=lr),
        metrics=[iou]
    )
    utils.plot_model(model, to_file=log_dir + "model.png",
                     show_shapes=True, show_layer_names=True)

    # Init callbacks
    pt = ProgressTracker()
    tensor_board = TensorBoard(log_dir=log_dir, histogram_freq=1, write_graph=False,
                               write_images=False, update_freq="epoch", embeddings_freq=1)
    checkpoint = ModelCheckpoint(log_dir + "weights.hdf5", monitor="val_iou",
                                 verbose=1, save_best_only=True, save_weights_only=True, mode="max")
    stop = EarlyStopping(monitor="val_iou", patience=EARLY_STOP, mode="max")
    reduce_lr = ReduceLROnPlateau(monitor="val_iou", factor=plateau_factor,
                                  patience=plateau_patience, min_lr=1e-7, verbose=1, mode="max")

    input_data, output_data = dataset.read_cached_detection_io()
    history = model.fit(
        x=input_data,
        y=output_data,
        batch_size=BATCH_SIZE,
        epochs=MAX_EPOCHS,
        verbose=1,
        callbacks=[checkpoint, reduce_lr, stop, tensor_board, pt],
        validation_split=0.2,
        shuffle=True
    )
    return history


if __name__ == "__main__":
    what_to_train = sys.argv[1]
    print(">> Training " + what_to_train)

    # Back up files for progress tracking
    log_dir = "{}/{}_{}/".format(LOG_DIR_BASE,
                                 datetime.now().strftime("%Y%m%d_%H%M%S"), what_to_train)
    Path(log_dir).mkdir(parents=True, exist_ok=False)
    os.system('cp train.py {}train.py'.format(log_dir))
    os.system('cp model.py {}model.py'.format(log_dir))

    if what_to_train == "position":
        history = train_position(
            log_dir=log_dir, plateau_patience=15, plateau_factor=0.5)
    elif what_to_train == "rotation":
        history = train_rotation(
            log_dir=log_dir, plateau_patience=15, plateau_factor=0.5)
    else:
        history = train_detection(
            log_dir=log_dir, lr=0.0005, plateau_patience=15, plateau_factor=0.2)

    print("Train complete.")
    input("Press Enter to continue...")

import dataset
import keras
import visualize
import model as mdl
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from keras import layers, utils
import numpy as np
from progress_tracker import ProgressTracker
import keras.backend as K

DATASET_DIR = "/mnt/storage_hdd/deep learning/datasets/basic_platform_test/"


def test():

    #====Detection====#
    samples = dataset.get_samples(DATASET_DIR)
    input_data, output_data = dataset.prepare_as_detection_io(samples)

    detection_module = mdl.detection_module()
    detection_module.load_weights(
        "training/logs/20200727_133134_detection/weights.hdf5")
    detection_module.summary()  # 187,284 params
    prediction = detection_module.predict(x=input_data)

    #visualize.plot_bboxes(samples, y_true=output_data, y_pred=prediction)
    dataset.write_predicted_bboxes(samples, prediction, DATASET_DIR)

    #====Pose estimation====#
    samples = dataset.get_samples(DATASET_DIR)
    input_data, output_data = dataset.prepare_as_posenet_io(samples)

    pose6d_module = mdl.pose6d_net(
        "training/logs/20200728_165212_position/weights.hdf5",
        "training/logs/20200729_103541_rotation/weights.hdf5")
    pose6d_module.summary()  # 3,450,279 params
    pred = pose6d_module.predict(x=input_data)

    #====Visualization====#
    # visualize.visualize_quaternion_losses(
    #     pred[1], output_data["quaternion_xyzw"])

    for i, s in enumerate(samples):
        visualize.render_prediction(s, pred[0][i], pred[1][i])


if __name__ == "__main__":
    test()
    input("Press Enter to continue...")

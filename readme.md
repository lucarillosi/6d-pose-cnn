# Installazione

Utilizzando python 3.6.x

```sh
pip install -r requirements.txt
```

### Ottimizzazione GPU Nvidia (opzionale)

1. Installa le dipendende specificate qui [TF GPU support](https://www.tensorflow.org/install/gpu#software_requirements)

al momento per setup ottimale (CUDA+TensorRT) le versioni corrette sono:

- cuda 10.1
- pycuda-headers

**TIP:** Downgrade di CUDA alla versione 10.1 con Pacman

```sh
sudo pacman -U https://archive.archlinux.org/packages/c/cuda/cuda-10.1.243-2-x86_64.pkg.tar.xz
```

poi

```sh
pip install pycuda
```

Se l'installazione da problemi assicurati che ci siano i path corretti nelle variabili ambiente necessarie. es per .bashrc:

```sh
export PATH=$PATH:/opt/cuda/bin/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/include/pycuda/
```

installa tensorrt seguendo [TensorRT install guide](https://docs.nvidia.com/deeplearning/sdk/tensorrt-install-guide/index.html#installing-tar)

NB: installa versione compatibile con CUDA 10.1 ad es: [TensorRT-6.0.1.5](https://developer.nvidia.com/compute/machine-learning/tensorrt/secure/6.0/GA_6.0.1.5/tars/TensorRT-6.0.1.5.Ubuntu-18.04.x86_64-gnu.cuda-10.1.cudnn7.6.tar.gz)

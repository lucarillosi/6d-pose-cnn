from keras.callbacks import Callback
import matplotlib.pyplot as plt


class ProgressTracker(Callback):
    def on_train_begin(self, logs={}):
        plt.rcParams["figure.figsize"] = (5, 8)
        self.stats = {
            'epoch': [], 'loss': [], 'val_loss': [],
            'lr': [], 'iou': [], 'val_iou': []
        }

    def on_epoch_end(self, epoch, logs):
        # Ignore first epochs
        if epoch < 2:
            return

        # log metrics
        self.stats["epoch"].append(epoch)
        self.stats["loss"].append(logs['loss'])
        self.stats["val_loss"].append(logs['val_loss'])
        self.stats["lr"].append(logs['lr'])

        if "iou" in logs:
            self.stats["iou"].append(logs['iou'])
            self.stats["val_iou"].append(logs['val_iou'])

        # plot metrics
        self.plot_progress()

    def plot_progress(self):
        plt.figure("live plot", figsize=None)
        plt.clf()

        plt.subplot(2, 2, 1)
        plt.plot(self.stats["epoch"], self.stats["val_loss"], label="val_loss")
        plt.plot(self.stats["epoch"], self.stats["loss"], label="loss")
        plt.legend()

        plt.subplot(2, 2, 2)
        plt.plot(self.stats["epoch"][-30:-1],
                 self.stats["val_loss"][-30:-1], label="val_loss")
        plt.plot(self.stats["epoch"][-30:-1],
                 self.stats["loss"][-30:-1], label="loss")
        plt.legend()

        plt.subplot(2, 2, 3)
        plt.plot(self.stats["epoch"], self.stats["lr"], label="lr")
        plt.legend()

        if "iou" in self.stats and len(self.stats["iou"]) > 0:
            plt.subplot(2, 2, 4)
            plt.plot(self.stats["epoch"],
                     self.stats["val_iou"], label="val_iou")
            plt.plot(self.stats["epoch"], self.stats["iou"], label="iou")
            plt.legend()

        plt.ion()
        plt.show()
        plt.pause(0.001)

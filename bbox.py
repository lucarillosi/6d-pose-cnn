import numpy as np
import warnings


class BBox():
    def __init__(self, bbox, import_format: str, img_width: int = None, img_height: int = None):
        self.y0 = self.x0 = self.y1 = self.x1 = None
        self.set_img_size(img_width, img_height)

        if import_format == "tf":
            self.from_tf(bbox)
        elif import_format == "classic":
            self.from_classic(bbox)
        else:
            raise Exception("invalid bbox import format")

    def from_tf(self, tf_bbox):
        """
        Import BBox from array expressed in Tensorflow format [y1, x1, y2, x2] where 0 <= value <= 1
        """
        self.y0, self.x0, self.y1, self.x1 = tf_bbox
        # self._check_warnings()

    def from_classic(self, classic_bbox):
        """
        Import BBox from array expressed in classic format [x1, y1, x2, y2] values in pixels
        if img_height is none, image is assumed to be a square of (img_width, img_width)
        """
        if not self._is_img_size_defined():
            raise Exception(
                "image size needs to be defined for importing classic bbox")

        self.x0 = classic_bbox[0] / self.img_width
        self.y0 = classic_bbox[1] / self.img_height
        self.x1 = classic_bbox[2] / self.img_width
        self.y1 = classic_bbox[3] / self.img_height
        # self._check_warnings()

    def from_yolo(self, yolo_bbox):
        """
        from yolo format [cx, cy, w, h] values in pixels
        """

        if not self._is_img_size_defined():
            raise Exception(
                "image size needs to be defined for importing yolo bbox")

        [cx, cy, w, h] = yolo_bbox
        x0 = cx - w / 2
        x1 = cx + w / 2
        y0 = cy - h / 2
        y1 = cy + h / 2

        self.from_classic([x0, y0, x1, y1])

    def to_tf(self):
        """
        Convert BBox in array expressed in Tensorflow format [y1, x1, y2, x2] where 0 <= value <= 1
        """
        return [self.y0, self.x0, self.y1, self.x1]

    def to_classic(self):
        """
        Convert BBox in array expressed in classic format [x1, y1, x2, y2] values in pixels
        """
        if not self._is_img_size_defined():
            raise Exception(
                "image size needs to be defined to convert to classic bbox")

        return [
            self.x0 * self.img_width,
            self.y0 * self.img_height,
            self.x1 * self.img_width,
            self.y1 * self.img_height,
        ]

    def to_yolo(self):
        """
        Convert BBox to array expressed in yolo format [cx, cy, w, h] values in pixels
        """
        cx, cy = self.get_center_px()
        w, h = self.get_width_px(), self.get_height_px()
        return [cx, cy, w, h]

    def get_height(self):
        """
        Get height expressed in % of image height
        """
        return abs(self.y0 - self.y1)

    def get_height_px(self):
        """
        Get height expressed in px
        """

        self._require_img_size_defined()
        return self.get_height() * self.img_height

    def get_width(self):
        """
        Get width expressed in % of image width
        """
        return abs(self.x0 - self.x1)

    def get_width_px(self):
        """
        Get width expressed in px
        """

        self._require_img_size_defined()
        return self.get_width() * self.img_width

    def get_center(self):
        """
        Get center coords expressed in % of image width
        """
        x_min = min(self.x0, self.x1)
        x_max = max(self.x0, self.x1)
        y_min = min(self.y0, self.y1)
        y_max = max(self.y0, self.y1)

        x = x_min + (x_max - x_min) / 2
        y = y_min + (y_max - y_min) / 2
        return x, y

    def get_center_px(self):
        """
        Get center coords expressed in % of image width
        """
        self._require_img_size_defined()
        x, y = self.get_center()
        return x * self.img_width, y * self.img_height

    def clip_to_image_bounds(self) -> None:
        """
        Clip bbox inside image bounds
        """
        bbox = self.to_tf()
        bbox = np.clip(bbox, 0.0, 1.0)
        self.from_tf(bbox)

    def set_img_size(self, img_width: int, img_height: int = None):
        """
        Set image size. Used to convert from classic to tf format and other utilities
        if img_height is none, image is assumed to be a square of (img_width, img_width)
        """
        # Assume square image if height is not provided
        img_height = img_height or img_width

        self.img_width = img_width
        self.img_height = img_height

    def resize_as_square(self):
        """
        Convert bbox into a square
        anchor point: center
        method: longest dimension
        """
        self._require_img_size_defined()
        cx, cy = self.get_center_px()
        l = max(self.get_width_px(), self.get_height_px())
        self.from_yolo([cx, cy, l, l])

    def _check_warnings(self):
        """
        Warns if bbox goes outside of image bounds
        """
        tf_bbox = self.to_tf()

        if min(tf_bbox) < 0 or max(tf_bbox) > 1:
            warnings.warn(
                "Imported bbox with coords outside image: " + str(self))

    def _is_img_size_defined(self):
        return self.img_height is not None and self.img_width is not None

    def _require_img_size_defined(self):
        if not self._is_img_size_defined():
            raise Exception("image size needs to be defined")

    def __repr__(self):
        return str({'x0': self.x0, 'y0': self.y0, 'x1': self.x1, 'y1': self.y1})

    def __str__(self):
        return repr(self)

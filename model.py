import keras
import keras.backend as K
from keras import layers
from keras.applications.mobilenet_v2 import MobileNetV2

#====== Detection ======#
# detection module RGB resolution (96, 128, 160, 192, 224)
RGB_IMG_SIZE = 96
#=======================#

#==== 6D Regression ====#
# 6D regression modules input depth img size
CROPPED_DEPTH_IMG_SIZE = 64
#=======================#


def detection_module():
    input_shape = (RGB_IMG_SIZE, RGB_IMG_SIZE, 1)
    x = input_tensor = keras.Input(shape=input_shape, name="depth")

    x = layers.Conv2D(16, (3, 3), padding="same", activation="relu")(x)
    jump = x
    x = layers.Conv2D(16, (3, 3), padding="same", activation="relu")(x)
    x = layers.Conv2D(16, (3, 3), padding="same", activation="relu")(x)
    x = layers.Conv2D(16, (3, 3), padding="same", activation="relu")(x)
    x = layers.Add()([x, jump])
    x = layers.MaxPooling2D(pool_size=2, strides=2)(x)

    x = layers.Conv2D(16, (3, 3), padding="same", activation="relu")(x)
    jump = x
    x = layers.Conv2D(16, (3, 3), padding="same", activation="relu")(x)
    x = layers.Conv2D(16, (3, 3), padding="same", activation="relu")(x)
    x = layers.Conv2D(16, (3, 3), padding="same", activation="relu")(x)
    x = layers.Add()([x, jump])
    x = layers.MaxPooling2D(pool_size=2, strides=2)(x)

    x = layers.Conv2D(32, (3, 3), padding="same", activation="relu")(x)
    jump = x
    x = layers.Conv2D(32, (3, 3), padding="same", activation="relu")(x)
    x = layers.Conv2D(32, (3, 3), padding="same", activation="relu")(x)
    x = layers.Conv2D(32, (3, 3), padding="same", activation="relu")(x)
    x = layers.Add()([x, jump])
    x = layers.MaxPooling2D(pool_size=2, strides=2)(x)

    x = layers.Conv2D(64, (3, 3), padding="same", activation="relu")(x)
    x = layers.Conv2D(64, (3, 3), padding="same", activation="relu")(x)
    x = layers.Conv2D(64, (3, 3), padding="same", activation="relu")(x)
    x = layers.Conv2D(64, (3, 3), padding="same", activation="relu")(x)
    x = layers.MaxPooling2D(pool_size=2, strides=2)(x)

    x = layers.Conv2D(4, (6, 6))(x)
    bbox = layers.Reshape((4,),  name="bbox")(x)

    return keras.Model(
        inputs=[input_tensor],
        outputs=bbox,
    )


def quaternion_activation(x):
    return K.l2_normalize(x, axis=-1)


def position_model():
    x = depth_input = keras.Input(
        shape=(CROPPED_DEPTH_IMG_SIZE, CROPPED_DEPTH_IMG_SIZE, 1), name="depth_cropped")

    # x = layers.GaussianNoise(0.02)(x)

    x = layers.Conv2D(16, (3, 3), padding="valid",
                      activation="selu")(depth_input)
    x = layers.Conv2D(16, (3, 3), padding="valid", activation="selu")(x)
    x = layers.MaxPooling2D(pool_size=2, strides=2)(x)

    x = layers.Conv2D(32, (3, 3), padding="valid", activation="selu")(x)
    x = layers.Conv2D(32, (3, 3), padding="valid", activation="selu")(x)
    x = layers.MaxPooling2D(pool_size=2, strides=2)(x)

    x = layers.Conv2D(32, (3, 3), padding="valid", activation="selu")(x)
    x = layers.Conv2D(32, (3, 3), padding="valid", activation="selu")(x)
    x = layers.MaxPooling2D(pool_size=2, strides=2)(x)

    x = layers.Flatten()(x)

    metadata_input = keras.Input(shape=(5,), name="crop_metadata")

    x = layers.concatenate([x, metadata_input])
    x = layers.Dense(128, activation="relu")(x)
    x = layers.Dense(128, activation="relu")(x)
    x = layers.Dense(128, activation="relu")(x)
    # x = layers.Dropout(0.2)(x)

    position = layers.Dense(3, name="centroid")(x)

    return keras.Model(
        inputs=[depth_input, metadata_input],
        outputs=position,
    )


def rotation_model():
    x = depth_input = keras.Input(
        shape=(CROPPED_DEPTH_IMG_SIZE, CROPPED_DEPTH_IMG_SIZE, 1), name="depth_cropped")

    # x = layers.GaussianNoise(0.02)(x)

    x = layers.Conv2D(16, (3, 3), padding="valid", activation="selu")(x)
    x = layers.Conv2D(16, (3, 3), padding="valid", activation="selu")(x)
    x = layers.MaxPooling2D(pool_size=2, strides=2)(x)

    x = layers.Conv2D(32, (3, 3), padding="valid", activation="selu")(x)
    x = layers.Conv2D(32, (3, 3), padding="valid", activation="selu")(x)
    x = layers.MaxPooling2D(pool_size=2, strides=2)(x)

    x = layers.Flatten()(x)

    metadata_input = keras.Input(shape=(5,), name="crop_metadata")

    x = layers.concatenate([x, metadata_input])
    x = layers.Dense(512, activation="relu")(x)
    x = layers.Dropout(0.3)(x)
    x = layers.Dense(512, activation="relu")(x)
    x = layers.Dropout(0.3)(x)
    x = layers.Dense(512, activation="relu")(x)
    x = layers.Dropout(0.3)(x)

    rotation = layers.Dense(4, name="quaternion_xyzw",
                            activation=quaternion_activation)(x)

    return keras.Model(
        inputs=[depth_input, metadata_input],
        outputs=rotation,
    )


def pose6d_net(position_model_weights, rotation_model_weights):
    depth_input = keras.Input(
        shape=(CROPPED_DEPTH_IMG_SIZE, CROPPED_DEPTH_IMG_SIZE, 1), name="depth_cropped")
    metadata_input = keras.Input(shape=(5,), name="crop_metadata")

    p = position_model()
    p.load_weights(position_model_weights)
    position = p([depth_input, metadata_input])

    r = rotation_model()
    r.load_weights(rotation_model_weights)
    rotation = r([depth_input, metadata_input])

    return keras.Model(
        inputs=[depth_input, metadata_input],
        outputs=[position, rotation],
    )

import math
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import tensorflow as tf
from mpl_toolkits.mplot3d import Axes3D
from open3d import camera, geometry, io, visualization
from pylab import *
from bbox import BBox
import dataset
from typing import List


def render_prediction(sample, pred_centroid=None, pred_quatern_xyzw=None, geometries=None):
    # Create point cloud
    color_img = io.read_image(sample['rgb'])
    depth_img = io.read_image(sample['depth_mm_16'])
    rgbd_img = geometry.RGBDImage.create_from_color_and_depth(
        color_img, depth_img, depth_scale=10.0, depth_trunc=512, convert_rgb_to_intensity=False)

    # Plot depth map with predicted bounding box
    _plot_image_with_bbox(depth_img, sample['pred_tf_bbox'])

    if geometries is None:
        geometries = []

    # Load point cloud
    pinhole_camera = camera.PinholeCameraIntrinsic(
        width=512, height=512, fx=256, fy=256, cx=256, cy=256)
    geometries.append(geometry.PointCloud.create_from_rgbd_image(
        rgbd_img, pinhole_camera))

    # Show ground truth
    target = geometry.TriangleMesh.create_coordinate_frame(
        size=32, origin=sample['objects'][0]['centroid'])
    target.rotate(geometry.get_rotation_matrix_from_quaternion(convert_quat_to_wxyz(
        sample['objects'][0]['quaternion_xyzw'])))  # quaternion in wxyz format!!
    geometries.append(target)

    # Show pose prediction
    if pred_centroid is not None:
        pred = geometry.TriangleMesh.create_coordinate_frame(
            size=32, origin=pred_centroid)
        rotation_matrix = geometry.get_rotation_matrix_from_quaternion(
            convert_quat_to_wxyz(pred_quatern_xyzw))
        pred.rotate(rotation_matrix)
        geometries.append(pred)
        bb = geometry.OrientedBoundingBox(
            pred_centroid, rotation_matrix, [25, 80, 75])
        bb.color = [0, 1, 0]
        geometries.append(bb)

    # Render scene
    vis = visualization.Visualizer()
    vis.create_window(window_name="Open3D - " +
                      sample['file_id'], width=800, height=800)
    for geo in geometries:
        vis.add_geometry(geo)
    view = vis.get_view_control()
    view.rotate(50, 1100)
    vis.run()


def convert_quat_to_wxyz(quat_xyzw):
    return np.roll(quat_xyzw, 1)


def _plot_image_with_bbox(depth_img, pred_tf_bbox: List[float]):
    # convert bbox to matplotlib format
    (w, h) = np.asarray(depth_img).shape
    bbox = BBox(pred_tf_bbox, import_format="tf", img_width=w, img_height=h)
    classic_bb = bbox.to_classic()
    corner = (classic_bb[0], classic_bb[1])
    bb_w, bb_h = bbox.get_width_px(), bbox.get_height_px()

    # Plot depth map
    plt.clf()
    ax = plt.subplot(1, 1, 1)
    ax.set_title('Depth map')
    ax.imshow(depth_img)
    rect = patches.Rectangle(
        corner, bb_w, bb_h, linewidth=1, edgecolor='r', facecolor='none')
    ax.add_patch(rect)
    plt.ion()
    plt.show()
    plt.pause(0.001)


def visualize_quaternion_losses(quats_xyzw, quats_xyzw_gt):
    points = [_quaternion_as_point_on_sphere(q) for q in quats_xyzw]

    xs = np.array(points)[:, 0]
    ys = np.array(points)[:, 1]
    zs = np.array(points)[:, 2]

    fig = plt.figure(figsize=(8, 6))

    ax = fig.add_subplot(111, projection='3d')

    losses = []
    for i in range(len(quats_xyzw_gt)):
        yt, yp = quats_xyzw_gt[i], quats_xyzw[i]
        loss = _simple_quaternion_loss(yt, yp)
        losses.append(loss)

    colors = cm.hsv(losses)

    colmap = cm.ScalarMappable(cmap=cm.hsv)
    colmap.set_array(losses)

    yg = ax.scatter(xs, ys, zs, c=colors, marker='.')
    cb = fig.colorbar(colmap)

    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')

    plt.show()


def _quaternion_as_point_on_sphere(quat_xyzw):
    p = [1, 0, 0]
    q = convert_quat_to_wxyz(quat_xyzw)
    return _point_rotation_by_quaternion(p, q)


def _quaternion_mult(q_wxyz, r):
    return [r[0]*q_wxyz[0]-r[1]*q_wxyz[1]-r[2]*q_wxyz[2]-r[3]*q_wxyz[3],
            r[0]*q_wxyz[1]+r[1]*q_wxyz[0]-r[2]*q_wxyz[3]+r[3]*q_wxyz[2],
            r[0]*q_wxyz[2]+r[1]*q_wxyz[3]+r[2]*q_wxyz[0]-r[3]*q_wxyz[1],
            r[0]*q_wxyz[3]-r[1]*q_wxyz[2]+r[2]*q_wxyz[1]+r[3]*q_wxyz[0]]


def _point_rotation_by_quaternion(point, q_wxyz):
    r = [0]+point
    q_conj = [q_wxyz[0], -1*q_wxyz[1], -1*q_wxyz[2], -1*q_wxyz[3]]
    return _quaternion_mult(_quaternion_mult(q_wxyz, r), q_conj)[1:]


def _simple_quaternion_loss(yt, yp):
    dot_prod = np.sum(yt * yp, axis=-1, keepdims=True)[0]
    p = (dot_prod**2) * 2 - 1
    p = p * 0.999
    q_angle_diff = math.acos(p)
    return q_angle_diff**2
